const cron = require('node-cron');
const axios = require('axios');

const scheduleTask = () => {
  cron.schedule('0 * * * *', async () => {
    const response = await axios.get(
      `${process.env.HOST}:${process.env.PORT}/news/update`
    );
    console.log(response.data);
  });
};

module.exports = { scheduleTask };
