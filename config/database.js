const mongoose = require('mongoose');
const logger = require('pino')();

const connectDB = async () => {
  try {
    await mongoose.connect(process.env.DATABASE_URI, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
  } catch (error) {
    logger.info(error);
  }
};

const disconnectDB = async () => {
  try {
    await mongoose.disconnect();
  } catch (error) {
    logger.info(error);
  }
};

module.exports = { connectDB, disconnectDB };
