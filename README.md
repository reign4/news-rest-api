# Karina Espinola Reign Application

This is a small REST API built with Node.js, Express, and MongoDB.

## Installation

1. Clone the repository from GitLab using this URL: [https://gitlab.com/reign4/news-rest-api.git](https://gitlab.com/reign4/news-rest-api.git)

2. Open a terminal in the root directory of the project and run the following command:
```bash
npm install
```
3. Create a file called .env in the root directory of the project with the following values:
```
PORT=3000
DATABASE_URI=mongodb+srv://karinaeh:hellohello@cluster0.n1bbc.mongodb.net/reign-rest-api?retryWrites=true&w=majority
HOST=http://localhost
```
4. Run the following command to get the server started
```bash
npm start
```

That's it! Now you have started the server and it's now ready to receive requests :)

## With docker-compose up
## Installation

1. Clone the repository from GitLab using this URL: [https://gitlab.com/reign4/news-rest-api.git](https://gitlab.com/reign4/news-rest-api.git)

2. Open a terminal in the root directory of the project and run the following command:
```bash
npm install
```

3. Create a file called .env in the root directory of the project with the following values:
```
PORT=3000
DATABASE_URI=mongodb+srv://karinaeh:hellohello@cluster0.n1bbc.mongodb.net/reign-rest-api?retryWrites=true&w=majority
HOST=http://localhost
```
4. Open a terminal in the root directory of the project and run the following command:
```bash
docker-compose up
```

Now the server is ready to receive requests! 
## Important notes:
* The database already has values, however, if you'd like to update it from the get-go and not wait until the next automatic update occurs then you can use the endpoint/news/update.
* Please take a look 👀 at the API documentation here: [https://app.swaggerhub.com/apis/karinaeh/reign/1.0.0](https://app.swaggerhub.com/apis/karinaeh/reign/1.0.0)
* You will find the exported Postman file in the root directory of the project, ready to be import to Postman to make tests :) 


Thank you! Have a great day!