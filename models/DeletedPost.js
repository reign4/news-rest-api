const mongoose = require('mongoose');

const { Schema } = mongoose;

const deletedPostSchema = new Schema({
  newsId: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model('DeletedPost', deletedPostSchema);
