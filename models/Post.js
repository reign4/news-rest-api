const mongoose = require('mongoose');

const { Schema } = mongoose;

const postSchema = new Schema(
  { created_at: { type: Date } },
  { strict: false }
);

module.exports = mongoose.model('Post', postSchema);
