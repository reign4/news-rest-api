require('dotenv').config();
const express = require('express');

const app = express();
const cors = require('cors');

// Allow CORS
app.use(cors());
// Built-in middleware to handle urlencoded
// in other words, form data;
// 'CONTENT-TYPE: application/x-www-form-urlencoded'
app.use(express.urlencoded({ extended: false }));

app.use(express.json());

app.get('/', (req, res) => {
  res.send('Welcome to the Node.js News API!');
});

// Routers
app.use('/news', require('./routes/api/news'));

module.exports = app;
