/* eslint-disable no-undef */
// eslint-disable-next-line node/no-unpublished-require
const request = require('supertest');
const app = require('../app');
const { connectDB, disconnectDB } = require('../config/database');

jest.setTimeout(10000);

beforeAll(() => {
  connectDB();
});

afterAll(() => {
  disconnectDB();
});

// BEGINNING OF GET TESTS

describe('GET /news', () => {
  test('Should respond with a 200 status code.', async () => {
    const response = await request(app).get('/news').send();
    expect(response.statusCode).toBe(200);
  });

  test('Should have json in its header.', async () => {
    const response = await request(app).get('/news').send();
    expect(response.headers['content-type']).toEqual(
      expect.stringContaining('json')
    );
  });

  test('Should return an object with news and pagination', async () => {
    const response = await request(app).get('/news');
    expect(response.body.news).toBeDefined();
    expect(response.body.pagination).toBeDefined();
  });
});

// END OF GET TESTS

// BEGINNING OF DELETE TESTS

describe('DELETE /news', () => {
  test('Should return status 400. Required field is empty.', async () => {
    const response = await request(app).delete('/news').send({});
    expect(response.statusCode).toBe(400);
  });
});

// END OF DELETE TESTS
