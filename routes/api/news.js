const express = require('express');

const router = express.Router();
const newsController = require('../../controllers/newsController');

router.get('/', newsController.getNews);
router.get('/update', newsController.updateNews);
router.delete('/', newsController.deleteNews);

module.exports = router;
