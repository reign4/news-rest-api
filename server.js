const mongoose = require('mongoose');
const logger = require('pino')();
const { connectDB } = require('./config/database');
const { scheduleTask } = require('./config/task');
const app = require('./app');

const { PORT } = process.env;
// Connect to MongoDB
connectDB();
// Schedule task to run every hour
scheduleTask();

mongoose.connection.once('open', () => {
  logger.info('Connected to MongoDB');
  app.listen(PORT, () => {
    logger.info(`Example app listening at http://localhost:${PORT}`);
  });
});
