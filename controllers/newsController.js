const logger = require('pino')();
const {
  checkNews,
  insertToDatabase,
  checkIfPostDeleted,
} = require('../helpers/news');
const Post = require('../models/Post');
const DeletedPost = require('../models/DeletedPost');

/**
 * Check the API endpoint if there are any new posts, if so then update the news database.
 */
const updateNews = async (req, res) => {
  try {
    const posts = await checkNews();
    let insertResult = { success: true };
    let arrayToInsert = [];
    // Get all deleted news
    const deletedPostsCollection = await DeletedPost.find({});
    const deletedPostArray = [];
    deletedPostsCollection.forEach((element) => {
      deletedPostArray.push(element.newsId);
    });
    const postsIdArray = [];
    posts.posts.forEach((element) => {
      postsIdArray.push(element.story_id);
    });
    // Get the latest post inserted in the database
    const latestPost = await Post.find().sort({ created_at: -1 }).limit(1);

    if (latestPost.length === 0) {
      // This means the database is empty
      arrayToInsert = posts.posts
        .map((postElement) => {
          const checkDeletedPost = deletedPostArray.find(
            (element) => postElement.story_id === element
          );
          console.log(checkDeletedPost);
          // Check is the post from the API is more recent than the latest in our database and also if this has not been deleted before
          if (checkDeletedPost === undefined) {
            return postElement;
          }
          return null;
        })
        .filter((element) => element !== null);
      insertResult = await insertToDatabase(arrayToInsert);
      console.log(arrayToInsert);
    } else {
      // Get the posts from the API that need to be added to the database because they are more recent than
      // the latest post in our database
      arrayToInsert = posts.posts
        .map((postElement) => {
          const latestPostDate = new Date(latestPost[0].created_at);
          const postElementDate = new Date(postElement.created_at);
          const checkDeletedPost = deletedPostArray.find(
            (element) => postElement.story_id === element
          );
          // Check is the post from the API is more recent than the latest in our database and also if this has not been deleted before
          if (
            postElementDate > latestPostDate &&
            checkDeletedPost === undefined
          ) {
            return postElement;
          }
          return null;
        })
        .filter((element) => element !== null);

      if (arrayToInsert.length > 0) {
        insertToDatabase(arrayToInsert);
      }
    }
    if (insertResult.success === false) {
      throw new Error(
        `There was an error this is what we know ${insertResult.error}`
      );
    }
    res.status(200);
    return res.json({ success: true, newRecords: arrayToInsert.length });
  } catch (error) {
    res.status(500);
    return res.json({ error: error.message });
  }
};

/**
 * Retrieves the records from the database that are compliant to the search params
 */
const getNews = async (req, res) => {
  try {
    // Get the search params from the request
    const { author, tags, title } = req.query;
    let { from = 1 } = req.query;
    from = parseInt(from, 10);
    const limit = 5;
    let pages = null;

    // Build search object
    const search = {};
    if (author !== undefined) {
      search.author = author;
    }
    if (tags !== undefined) {
      const tagsArray = tags.split(',');
      // eslint-disable-next-line dot-notation
      search['_tags'] = { $in: tagsArray };
    }
    if (title !== undefined) {
      search.title = title;
    }
    // Search in the database
    const news = await Post.find(search)
      .sort({ created_at: 'desc' })
      .skip(from)
      .limit(limit);

    const totalNews = await Post.count(search);
    pages = Math.ceil(totalNews / limit);
    res.status(200);

    return res.json({
      success: true,
      news,
      pagination: { pages, newsPerPage: limit, from, totalNews },
    });
  } catch (error) {
    res.status(500);
    return res.json({ success: false, error });
  }
};

/**
 * Deletes a news post given a newsId
 */
const deleteNews = async (req, res) => {
  try {
    const { newsId } = req.body;
    if (newsId === undefined || newsId === null) {
      throw new Error(`The newsId field is required`);
    }

    // Delete from the database
    const deleteResponse = await Post.findOneAndDelete({
      story_id: newsId,
    }).exec();
    // The response message will depend on if it was possible to delete any records in the database
    let responseMessage = `News #${newsId} was deleted successfully!`;

    if (deleteResponse === null) {
      responseMessage = `News #${newsId} was not found. No records were deleted.`;
    } else {
      // Insert in the database the record was deleted to avoid showing it again
      await DeletedPost.create({ newsId: deleteResponse.story_id });
    }
    res.status(200).send({ message: responseMessage });
  } catch (error) {
    res.status(400);
    res.send({ error: error.message });
  }
};

module.exports = { updateNews, getNews, deleteNews };
