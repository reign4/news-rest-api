FROM node:17-alpine3.14
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "server.js"]