const axios = require('axios').default;
const Post = require('../models/Post');
const DeletedPost = require('../models/DeletedPost');

const API_URL = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
/**
 * Get the most recet news from the hn.algolia API
 * @returns {Array} The most recent news
 */
const checkNews = async () => {
  try {
    // Make request to the API to later compare with the posts in our database
    const newsApi = await axios.get(API_URL);
    if (newsApi.data !== undefined && newsApi.data.hits.length > 0) {
      return { success: true, posts: newsApi.data.hits };
    }
    return { success: true, posts: null };
  } catch (error) {
    return { success: false, error };
  }
};

/**
 *
 * @param {Array} posts Posts to be inserted to the database
 */
const insertToDatabase = async (posts) => {
  try {
    await Post.insertMany(posts);
    return { sucess: true };
  } catch (error) {
    return { sucess: false, error };
  }
};

/**
 * Checks if a given post is among the records of the deleted posts in the database
 * @param {Object} post Object
 * @returns Object if it finds it or undefined if it didn't find it among the deleted records
 */
const checkIfPostDeleted = (post, deletedPostArray) => {
  const result = deletedPostArray.find((element) => post.story_id === element);
  console.log(result);
  if (result === undefined) {
    return false;
  }
  return true;
};

module.exports = { checkNews, insertToDatabase, checkIfPostDeleted };
